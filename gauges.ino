//
// weastat.ino --- wind/rain gauges to serial interface
//

#define G 11               // raindrops pin
#define A 12               // anemometer pin

static char wea[128];      // weather string data
int wea_idx = 0;           // where to put next char

void setup() {
  Serial.begin(115200);

  // LED
  //
  pinMode(13, OUTPUT);
  
  // rain drops counter
  //
  pinMode(G, INPUT_PULLUP); // drops (red cable)
  pinMode(9, OUTPUT);       // drops GND (green cable)
  digitalWrite(9, 0);       // "assert GND" for pull-up

  // anemometer
  //
  pinMode(A, INPUT_PULLUP); // anemometer (blue cable)
  pinMode(5, OUTPUT);       // anemometer GND (green cable)
  digitalWrite(5, 0);       // "assert GND" for pull-up

  delay(100);
}


int a = 0;                  // latest anemometer state
int g = 0;                  // latest rain drops count

void loop() {
  int x = 0, totg = 0, tota = 0, debounce = 0;

  digitalWrite(13, !digitalRead(13));

  // ~512msec loop before status output
  //
  while((millis() & 0x1ff) != 0)
  {
    // fetch incoming data from the Sparkfun SEN-08311 v2 weather board:
    //
    while(Serial.available())
    {
      char c = Serial.read() & 0x7f;
      wea[wea_idx++] = c;
      if(c == '\n' || wea_idx == sizeof(wea)-1)
      {
        wea[wea_idx++] = 0;
        Serial.print(wea);  // instant output, no regrets
        wea_idx = 0;
      }
    }

    if(!debounce)           // ready to read?
    {
      x = digitalRead(G);
      if(g != x)            // incoming drop?
      {
        totg++;
        g = x;
        debounce = 7;       // 7msec debounce time for next loops
      }
    
      x = digitalRead(A);
      if(a != x)            // did anemometer cups rotate?
      {
        tota++;
        a = x;
        debounce = 7;       // 7msec debounce time for next loops
      }
    }
    
    if(debounce > 0)        // are we just waiting for sensors to settle?
    {
      delay(1);
      debounce--;
    }
  }

  x = analogRead(5);        // fetch wind direction

  Serial.print(totg);       // output record: raindrops, windspeed, wind direction
  Serial.print("\t");
  Serial.print(tota);
  Serial.print("\t");
  Serial.print(x);
  Serial.println();
}

// ---
