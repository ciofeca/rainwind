# Description

Collect data from Sparkfun [SEN-08942](https://www.sparkfun.com/products/8942) weather meters (windspeed, direction, and rain) on a Beagleboard/Beaglebone board using GPIO and Analog input kernel support, logging every second the number of rain drops, the number of windspeed ticks (4 of them sum up to 0.667 m/sec) and the wind direction (0 to 15 representing "relative North" to "relative North-North-West" depending on the physical orientation of the contraption).

On a 720 MHz Beaglebone Original ("white") the Ruby script requires as low as 0.1% CPU time. It should work on any Beaglebone-like board featuring GPIOs and *in_voltage0_raw*-like analog inputs (you may need the "cape trick" as explained [here](https://www.teachmemicro.com/beaglebone-black-adc/)). Note that GPIO initialization requires root privileges.

Also tested on LPC1768 boards. An Arduino sketch is also available.

## Installation

Both Beagleboard and Beaglebone use 1.8V GPIOs.

The anemometer values are calculated on a ~12k resistor (not a ~10k as in the Arduino examples by other people). Use the *test-wind-direction-finder.rb* script to find out the 16 direction values (min/max) and sort them by index. For example, on the Beaglebone (12 bit ADC, 0 to 1.8V range) the wind direction lowest values here were 206 to 224, the next to lowest one showed 267 to 285; I chose 245 as a safe mid-value between the max value of direction 0 (relative "North") and the min value of direction 1 (relative "North-North-East").

My SEN-8942 is an old one featuring two reed switches in the anemometer giving four GPIO events (rise/fall + rise/fall interrupts), this is why I need 4 ticks to get a reliable speed value.

### Beaglebone specific notes

The weather meters come with two cables: the rain gauge one (two pins to be connected to GPIO 67, aka P8.8, and GND, aka P8.1 or P8.2) and the wind group one (four pins, to be connected to AIN GND aka P9.34, GPIO 68 aka P8.10, GND aka P9.1 or P9.2, and AIN 0 aka P9.39- the latter on the resistor divider circuit connected to AIN VDC aka P9.32 and a ~12k resistor).

### Beagleboard xM specific notes

The Beagleboard xM only features a single ADC input coming from the TPS65950 (0 to ~2.4V) connected to the *in_voltage6_raw* on the pin 17 of the P17 connector (the one below, near the ethernet port). It does not need the "cape trick".

The rain gauge pins have to be connected to GPIO 139 (aka P9.3) and GND (aka P9.27 or P9.28). The wind group pins have to be connected to GND (aka P17.19 or P17.20), GPIO 138 (aka P9.5), GND (aka P9.27 or P9.28) and AUX-ADC (P17.17). The 

### LPC-1768 based boards specific notes

Pins are actually 3.3V, you have to re-calculate wind direction table values.

### Arduino specific notes

The *gauges.ino* sketch sends a status line (raindrops, windspeed, direction) every ~512 msec over its serial port.

Note that the direction field is a raw analog read that the host has to parse. Tested on Arduino Duemilanove; you will have to re-calculate wind direction table values if you use a 3.3V Arduino or clone.

## Extra

Italian description [here](https://particolarmente-urgentissimo.blogspot.com/2018/01/hmm-piaceri-e-dolori.html).
