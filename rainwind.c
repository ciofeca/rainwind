//
//      rain and wind event collector
//

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <poll.h>
#include <unistd.h>
#include <time.h>

// --- configuration
int timeout = 100;            // max poll wait (milliseconds)
int debounc = 5;              // debouncing time (milliseconds)

// --- anemometer
const int vol[16] = { 245, 293, 367, 525, 733, 917, 1215, 1549, 1931, 2269, 2483, 2811, 3087, 3317, 3585, 4095 };
const int eff[16] = { 9,   7,   8,   11,  10,  13,  12,   5,    6,    15,   14,   3,    4,    1,    2,    0    };

// --- globals
static struct pollfd po[6];  // GPIOs to poll
static int nfd = 0;          // number of active GPIOs


void eexit(int e, char* msg)
{
  fprintf(stderr, "E%d: ", e);
  perror(msg);
  exit(e);
}


int winddir()
{
  int r = 4096;
  int n;
  FILE* fp = fopen("/sys/bus/iio/devices/iio:device0/in_voltage0_raw", "r");
  if(fp == NULL) eexit(31, "cannot read in_voltage0_raw: bone cape analog not loaded? check this: www.teachmemicro.com/beaglebone-black-adc/");
  fscanf(fp, "%d", &r);
  if(r < 0 || r >= 4096) eexit(32, "error reading in_voltage0_raw: new kernel?");
  for(n=0; n<16; n++) if(r<=vol[n]) break;
  fclose(fp);
  return eff[n];
}


void config(int gpio)
{
  if(nfd >= 6) eexit(20, "this version only supports 6 GPIOs max");

  char fname[256];
  FILE* fp = fopen("/sys/class/gpio/export", "w");
  if(fp == NULL) eexit(21, "cannot write to ...gpio/export: unsupported kernel?");
  fprintf(fp, "%d\n", gpio);
  fclose(fp);

  sprintf(fname, "/sys/class/gpio/gpio%d/edge", gpio);
  fp = fopen(fname, "w");
  if(fp == NULL) eexit(22, "cannot access ...gpio/edge file: unsupported kernel?");
  fprintf(fp, "both\n");
  fclose(fp);

  sprintf(fname, "/sys/class/gpio/gpio%d/value", gpio);
  po[nfd].fd = open(fname, O_RDONLY);
  if(po[nfd].fd < 0) eexit(23, "cannot open ...gpio/value file: no root permissions?");

  po[nfd].events = POLLPRI;
  po[nfd].revents = 0;
  nfd++;
}


int main(int argc, char *argv[])
{
  char chr = 0;
  int err = 0;
  int n = 0;
  time_t t = 0;
  time_t lastt = 0;
  int ev[6] = { 0,0,0, 0,0,0 };

  config(67);  // P8.8   - right side, red cable -- "rain drops"
  config(68);  // P8.10  - right side, blue cable -- "wind speed"

  for(;;)
  {
    time(&t);
    if(t != lastt)
    {
      if(lastt) printf("> r%d s%d d%d\n", ev[0], ev[1], winddir());
      for(n=0; n<6; n++) ev[n] = 0;
      lastt = t;
    }

    err = poll(po, nfd, timeout);
    if(err < 0) eexit(11, "poll");

    for(n=0; n<nfd; n++)
    {
      if(po[n].revents)
      {
        usleep(debounc * 1000);
        lseek(po[n].fd, SEEK_SET, 0l);
        read(po[n].fd, &chr, 1);
        ev[n]++;
      }
    }
  }
}

